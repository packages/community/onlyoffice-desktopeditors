# Maintainer: Philip Mueller <philm[at]manjaro[dot]org>
# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Helmut Stult
# Contributor: Daniel Bermond <dbermond@archlinux.org>
# Contributor: Robin Candau <antiz@archlinux.org>
# Contributor: Mikalai Ramanovich < narod.ru: nikolay.romanovich >

pkgname=onlyoffice-desktopeditors
pkgver=8.3.1
pkgrel=1
pkgdesc="Open-source office suite that combines text, spreadsheet and presentation editors."
arch=('x86_64')
url="https://www.onlyoffice.com/"
license=('AGPL-3.0-or-later')
depends=(
  'alsa-lib'
  'curl'
  'desktop-file-utils'
  'gst-plugins-base-libs'
  'gst-plugins-ugly'
  'gstreamer'
  'gtk3'
  'hicolor-icon-theme'
  'libpulse'
  'libxss'
  'nspr'
  'nss'
  'ttf-carlito'
  'ttf-dejavu'
  'ttf-liberation'
)
optdepends=(
  'gst-plugins-good: for playing embedded video files'
  'libreoffice: for OpenSymbol fonts'
#  'otf-takao: for Japanese Takao fonts' # Only available in the AUR
#  'ttf-ms-fonts: for Microsoft fonts' # Only available in the AUR
)
provides=('onlyoffice')
conflicts=('onlyoffice')
source=("${pkgname}-${pkgver}_amd64.deb::https://github.com/ONLYOFFICE/DesktopEditors/releases/download/v${pkgver}/${pkgname}_amd64.deb"
        '010-fix-document-opening.patch')
noextract=("${pkgname}-${pkgver}_amd64.deb")
sha256sums=('e9ea172ce2ec847a67dde684c79ee597aea70fe82cd4b654113d024a6e287797'
            '670de5f8b72679a54ff41a96ba1bdba9231a93260d1a8eaf304f66c8e40efdb7')

prepare() {
  mkdir -p "onlyoffice-${pkgver}/pkg"
  bsdtar -xf "${pkgname}-${pkgver}_amd64.deb" -C "onlyoffice-${pkgver}"
  bsdtar -xf "onlyoffice-${pkgver}/data.tar.xz" -C "onlyoffice-${pkgver}/pkg"
  patch -d "onlyoffice-${pkgver}/pkg" -Np1 -i "${srcdir}/010-fix-document-opening.patch"
}

package() {
  cp -dr --no-preserve='ownership' "onlyoffice-${pkgver}"/pkg/* "$pkgdir"

  # icons
  local _file
  local _res
  while read -r -d '' _file
  do
    _res="$(sed 's/\.png$//;s/^.*-//' <<< "$_file")"
    install -d -m755 "${pkgdir}/usr/share/icons/hicolor/${_res}x${_res}/apps"
    ln -s "../../../../../../opt/onlyoffice/desktopeditors/asc-de-${_res}.png" \
      "${pkgdir}/usr/share/icons/hicolor/${_res}x${_res}/apps/${pkgname}.png"
  done < <(find "${pkgdir}/opt/onlyoffice/desktopeditors" -maxdepth 1 -type f -name 'asc-de-*.png' -print0)
}
